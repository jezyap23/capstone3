import { useState , useEffect } from 'react';
import {Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function GetAllProductCard({productProp}){

	const {name, description, price, _id} = productProp;
	return(

		<Row className="mt-3 mb-3">
			<Col xs={12} md={6}>
				<Card className="prodCard p-3">
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <h6>Description</h6>
				        <p>{description}</p>
				        <h6>Price:</h6>
				        <h6>PhP {price}</h6>
				        
				        <Button className="bg-primary" as={Link} to= {`/singleProduct/${_id}`}>Details</Button>
				      </Card.Body>
				    </Card>
			</Col>
		</Row>

	)

}