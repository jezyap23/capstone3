import { useState, useEffect, useContext } from 'react';
import { Conatiner, Button, Row, Col, Form } from 'react-bootstrap';
import {useParams, useNavigate, Link } from 'react-router-dom';
import Admin from '../pages/Admin';
import AddProducts from '../pages/AddProducts';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function UpdateProduct() {

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const { productId } = useParams();

	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price, setPrice ] = useState(0);

	const [isActive, setIsActive ] = useState(false);

	// function updateProd(e){
	// 	e.preventDefault()
	// }

	const update = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`,{
			method: "PUT",
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true ){
				Swal.fire({
					title: "Successfully Update",
					icon: "success",
					text: "You have updated product Successfully"
				})

					// navigate("/adminDashboard")
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	};

	useEffect(()=>{
		console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)

			if(name !== null && description !== null && price !==null){
				setIsActive(true)
			} else {
				setIsActive(false)
			}

		})
	},[productId])


	return(
		<Form onSubmit= {e => update(e)}>

				<Form.Group className="mb-3" controlId="Name">
				  <Form.Label>Product Name</Form.Label>
				  <Form.Control 
				  	type="name" 
				  	placeholder="{name}"
				  	value={name}
				  	onChange={e => setName(e.target.value)}
				  	required
				  />
				</Form.Group>

				<Form.Group className="mb-3" controlId="description">
				  <Form.Label>Description</Form.Label>
				  <Form.Control 
				  	type="description" 
				  	placeholder="{description}"
				  	value={description}
				  	onChange={e => setDescription(e.target.value)}
				  	required
				  />
				</Form.Group>

				<Form.Group className="mb-3" controlId="price">
				  <Form.Label>Price</Form.Label>
				  <Form.Control 
				  	type="price" 
				  	placeholder="{price}"
				  	value={price}
				  	onChange={e => setPrice(e.target.value)}
				  	required
				  />
				</Form.Group>

				


		      {
		      	isActive ?
		      		<Button variant="primary" type="submit" id="submitBtn">
		      		  Submit
		      		</Button>
		      		:
		      		<Button variant="primary" type="submit" id="submitBtn" disabled>
		      		  Submit
		      		</Button>
		      }


		    </Form>

	)
}
































